﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Films</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
<body>  
 <div class="container">
 <h2> Список фильмов </h2>
 
 <table class="table table-hover">
 <thead>
 <tr>
 <th>id</th>
 <th>Название</th>
 <th>Год</th>
 </tr>
 </thead>
 
  
<?php

include('connect.php');


$sql = "SELECT id, Name, Year FROM films WHERE isActive='1'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo "<tr><td>  ". $row["id"]. " </td> <td> ". $row["Name"]. "</td> <td> " . $row["Year"] . "</td></tr>";
     }
} else {
     echo '
	 <div class="alert alert-warning">
     <strong>Пусто!</strong> К сожалению нет результатов.
     </div>
	 ';
}
    
 ?>
  
  </table>
  <a href="add_film.php" class="btn btn-default" role="button">Добавить фильм</a>
  </div>
  
 <footer class="container-fluid text-center">
  <p>Made by Sergiy Kavunenko</p> 
</footer>
  
   

</body>
</html>